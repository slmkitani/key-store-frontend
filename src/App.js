import './App.css';
import 'antd/dist/antd.css';
import Test from './components/test'

function App() {
  return (
    <div className="App">
      <Test />
    </div>
  );
}

export default App;
